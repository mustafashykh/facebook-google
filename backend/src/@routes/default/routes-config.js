const express = require('express');
const router = express.Router();
const controller = require('./controller');
const converter = require('json-2-csv');

// Add all routes here
// router.get('', controller.default);

router.post('/get-csv',(req,res)=>{
	// let body = [{name:"Mustafa" , age:25}]
	let body = req.body
	converter.json2csv(body, (err, csv) => {
		if (err) {
			throw err
		}
		// console.log(csv)
		res.attachment('data.csv')
		res.type('.csv')
		res.status(200)
		res.send(csv)

	})

})

module.exports = router;
