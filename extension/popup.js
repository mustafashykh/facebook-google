$(document).ready(async function () {

    $("#extract-maps").click(function () {

        $('#load').css("display", "block")
        let type = $("#type").val()
        let query = $("#query").val().trim()
        if (type && query) {

            $('#errorMessage').css("display", "none")

            chrome.tabs.query({ active: true, currentWindow: true }, (tabs) => {
                chrome.tabs.sendMessage(tabs[0].id, { type, query, searchMap: true })
            })


        }
    })

    $("#extract-google").click(function () {
        let query = $("#query").val().trim()
        let type = $("#type").val()
        if (type && query) {
            chrome.tabs.query({ active: true, currentWindow: true }, (tabs) => {
                chrome.tabs.sendMessage(tabs[0].id, { searchGoogle: true, query })
            })
        }

    })

    // $("#extract-facebook").click(function () {

    //     chrome.tabs.query({ active: true, currentWindow: true }, (tabs) => {
    //         chrome.tabs.sendMessage(tabs[0].id, { searchFacebook: true })
    //     })

    // })
})