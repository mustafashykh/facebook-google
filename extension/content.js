//
const ngrokLink = "https://2c20-202-166-171-14.ngrok.io"
chrome.runtime.onMessage.addListener(async ({ type, query, searchMap }) => {
  if (!searchMap) return

  var overlayDiv = document.createElement('div')
  overlayDiv.setAttribute('id', "overlay")
  overlayDiv.style.cssText = 'position:absolute;width:100%;height:100%;opacity:0.3;z-index:100;background:#000;'
  document.body.appendChild(overlayDiv);

  const searchInput = document.getElementById('searchboxinput')
  searchInput.value = `${type} ${query}`
  searchInput.dispatchEvent(ev)

  const dataAll = []
  let restaurantsCount = 0
  await delay(12000)
  let allRestaurants = document.querySelectorAll('div[role="article"]')
  let hasNewRestaurants = true
  const limit = 200
  while (hasNewRestaurants && restaurantsCount < limit && allRestaurants.length >= restaurantsCount) {
    if (!allRestaurants[restaurantsCount]) break
    allRestaurants[restaurantsCount].querySelector('a').click()
    await delay(5000)
    try {
      const resName = document.querySelector('div[role="main"] .fontHeadlineLarge span').innerText
      // console.log(resName)

      const resPlusCode = document.querySelector('div[role="main"] img[src="//maps.gstatic.com/mapfiles/maps_lite/images/2x/ic_plus_code.png"]').closest('button').innerText


      let timings = null
      const allButtons = [...document.querySelectorAll('div[role="main"] div[role="button"]')]

      for (let i = 0; i < allButtons.length; i++) {
        let img = allButtons[i].querySelector('div[role="main"] img[src="//www.gstatic.com/images/icons/material/system_gm/2x/schedule_gm_blue_24dp.png"]')
        if (!img) {
          img = allButtons[i].querySelector('div[role="main"] img[src="//www.gstatic.com/images/icons/material/system_gm/1x/schedule_gm_blue_24dp.png"]')
        }
        if (img) {
          img.click()
          await delay(1000)
          let timing = document.querySelector('div[role="main"] div[aria-label*="Hide open hours for the week"]').getAttribute('aria-label')
          if (timing) {
            timings = timing.split('Hide')[0]
          }
        }
      }
      try {
        if (!timings) {

          let img = document.querySelector('div[role="main"] img[src="//www.gstatic.com/images/icons/material/system_gm/2x/schedule_gm_blue_24dp.png"]')
          if (!img) {
            img = document.querySelector('div[role="main"] img[src="//www.gstatic.com/images/icons/material/system_gm/1x/schedule_gm_blue_24dp.png"]')
          }

          if (img && img.closest('button')) {
            img.closest('button').click()
            await delay(1000)
            let timing = document.querySelector('div[role="main"] div[aria-label*="Hide open hours for the week"]').getAttribute('aria-label')
            if (timing) {
              timings = timing.split('Hide')[0]
            }
          }
        }
      }
      catch (e) {
        console.log(e)
      }

      // if clicking on hours takes to another screen on open modal section and other data is on previous screen
      const backBtn = document.querySelector('button[aria-label="Back"]')
      if (backBtn) {
        backBtn.click()
        await delay(1000)
      }

      const addressExists = document.querySelector('div[role="main"] button[data-item-id*="address"]')
      let resAddress = null
      if (addressExists) {
        resAddress = document.querySelector('div[role="main"] button[data-item-id*="address"] .rogA2c .fontBodyMedium').innerText
      }

      // Get menu link
      const menuExists = document.querySelector('div[role="main"] a[data-tooltip*="menu"]')
      let resMenuLink = null
      if (menuExists) {
        resMenuLink = menuExists.getAttribute('href')
      }

      const websiteExists = document.querySelector('div[role="main"] a[data-tooltip*="website"]')
      let resWebsite = null
      if (websiteExists) {
        resWebsite = websiteExists.getAttribute('href')
      }

      const phoneExists = document.querySelector('div[role="main"] button[data-tooltip*="phone"]')
      let resPhone = null
      if (phoneExists) {
        resPhone = phoneExists.querySelector('.rogA2c .fontBodyMedium').innerText
      }

      dataAll.push({
        'Name': resName,
        'Address': resAddress,
        'MenuLink': resMenuLink,
        'Website': resWebsite,
        'Phone': resPhone,
        'Timings': timings,
        'PlusCode': resPlusCode,
      })
    }
    catch (e) { console.log(e) }

    restaurantsCount++
    if (!allRestaurants[restaurantsCount]) {
      let count = 0
      document.querySelector('div[role="feed"]').scrollBy(1, 1000)
      await delay(5000)
      while (count <= 1) {
        let newRestaurants = document.querySelectorAll('div[role="article"]')
        count += 1
        // console.log("QUERY -- ", [...newRestaurants].map((res) => res.querySelector('a').getAttribute('href')))
        if (newRestaurants) {
          newRestaurants = [...newRestaurants]
          newRestaurants = [...newRestaurants.slice(restaurantsCount)]
          // console.log("NEW Restaurant -- ", newRestaurants)
          allRestaurants = [...allRestaurants, ...newRestaurants]
          if (!newRestaurants.length) {
            document.querySelector('div[role="feed"]').scrollBy(1, 1000)
            await delay(5000)
            document.querySelector('div[role="feed"]').scrollBy(1, 500)
            continue
          }
          break
        }

      }


    }
  }

  console.log('Restaurants --->>>>', dataAll)

  try {
    await chrome.storage?.sync.set({ restaurants: [...dataAll] })

  }
  catch (e) {

  }

  const allLinks = []
  try {
    for (let dataIndex = 0; dataIndex < dataAll.length; dataIndex++) {
      const element = dataAll[dataIndex]
      let queryHtml = await fetch(`https://www.google.com/search?q=facebook ${element.Name} ${query}`)
      queryHtml = await queryHtml.text()

      var parser = new DOMParser()
      var htmlDoc = parser.parseFromString(queryHtml, 'text/html')
      const a = htmlDoc.querySelector('a[href*="www.facebook.com"]')
      const href = `${a.getAttribute('href')}about`
      allLinks.push(href)

    }
  }

  catch (e) { console.log(e) }

  let dataForCsv = [...dataAll]

  if (allLinks.length) {
    dataForCsv = [...dataAll.map((res, ind) => ({ ...res, Facebook: allLinks[ind] }))]
  }

  const myHeaders = new Headers()
  myHeaders.append("Content-Type", "application/json")

  const body = JSON.stringify(dataForCsv)

  var requestOptions = {
    method: 'POST',
    headers: myHeaders,
    body,
    redirect: 'follow'
  }

  // NGROK link needs to be change

  try {
    let csv = await fetch(`${ngrokLink}/get-csv`, requestOptions)
    csv = await csv.text()
    const blob = new Blob([csv], { type: 'text/csv' })

    // Creating an object for downloading url
    const url = window.URL.createObjectURL(blob)

    // Creating an anchor(a) tag of HTML
    const a = document.createElement('a')

    // Passing the blob downloading url
    a.setAttribute('href', url)

    // Setting the anchor tag attribute for downloading
    // and passing the download file name
    a.setAttribute('download', 'data.csv')

    // Performing a download with click
    a.click()

    alert("Congratulations!\nData from google maps have been extracted.")
  }
  catch (e) { console.log(e) }
  overlayDiv.remove()
})


chrome.runtime.onMessage.addListener(async ({ query, searchGoogle }) => {
  if (searchGoogle) {
    let { restaurants } = await chrome.storage.sync.get(["restaurants"])
    console.log("RESTAURANTS in storage -- ", restaurants)
    if (!restaurants.length) return

    var myHeaders = new Headers()
    myHeaders.append("Content-Type", "application/json")

    var raw = JSON.stringify(restaurants)

    var requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: raw,
      redirect: 'follow'
    }

    let csv = await fetch(`${ngrokLink}/get-csv`, requestOptions)
    csv = await csv.text()
    const blob = new Blob([csv], { type: 'text/csv' })

    // Creating an object for downloading url
    const url = window.URL.createObjectURL(blob)

    // Creating an anchor(a) tag of HTML
    const a = document.createElement('a')

    // Passing the blob downloading url
    a.setAttribute('href', url)

    // Setting the anchor tag attribute for downloading
    // and passing the download file name
    a.setAttribute('download', 'download.csv')

    // Performing a download with click
    a.click()

  }

})



function delay(ms) {
  return new Promise(resolve => setTimeout(resolve, ms))
}

var ev = new KeyboardEvent('keydown', {
  altKey: false,
  bubbles: true,
  cancelBubble: false,
  cancelable: true,
  charCode: 0,
  code: "Enter",
  composed: true,
  ctrlKey: false,
  currentTarget: null,
  defaultPrevented: true,
  detail: 0,
  eventPhase: 0,
  isComposing: false,
  isTrusted: true,
  key: "Enter",
  keyCode: 13,
  location: 0,
  metaKey: false,
  repeat: false,
  returnValue: false,
  shiftKey: false,
  type: "keydown",
  which: 13
})