# Google Maps Data

Web application is dedicated to extract places data from google maps.

![Screenshot_2022-12-02_at_8.14.56_PM](/uploads/feccb93c316fc1a5aebaeb418082de83/Screenshot_2022-12-02_at_8.14.56_PM.png)

## Functionalities

- Extract places data from google maps
    - Name.
    - Address.
    - Timings.
    - Menu Link.
    - Website Link.
    - Phone #.

- Get Facebook URL from google
    - URL to the about section of facebook page.

- Download data in CSV file

## How to add and run in Chrome

- Go to extensions from more tools.
- Load unpacked extension folder.
- Run backend server for downloading CSV file.
- An ngrok link needs to be change in the top of content.js file in extension folder.
- Go to google maps and select place type from extension already added in chrome and type text i.e Restaurants in 10001.

## Tutorial
[Tutorial](https://www.youtube.com/watch?v=i_gtrf3I-9s)
